package com.techprimers.db.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
public class WalletTransaction {
    @Id
    @GeneratedValue
    @Column(name = "id")
    private Integer id;

    @Column(name = "fromWallet")
    @JsonProperty("from")
    private String fromWallet;

    @Column(name = "toWallet")
    @JsonProperty("to")
    private String toWallet;

    @Column(name = "timeStamp")
    @JsonProperty("timeStamp")
    private Long timeStamp;

    @Column(name = "value")
    @JsonProperty("value")
    private String value;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "wallet_id")
    private Wallet wallet;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFromWallet() {
        return fromWallet;
    }

    public void setFromWallet(String fromWallet) {
        this.fromWallet = fromWallet;
    }

    public String getToWallet() {
        return toWallet;
    }

    public void setToWallet(String toWallet) {
        this.toWallet = toWallet;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Wallet getWallet() {
        return wallet;
    }

    public void setWallet(Wallet wallet) {
        this.wallet = wallet;
    }

    public WalletTransaction() {
    }

    public WalletTransaction(String fromWallet, String toWallet, Long timeStamp, String value, Wallet wallet) {
        this.fromWallet = fromWallet;
        this.toWallet = toWallet;
        this.timeStamp = timeStamp;
        this.value = value;
        this.wallet = wallet;
    }
}
