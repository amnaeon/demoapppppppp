package com.techprimers.db.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import com.techprimers.db.model.WalletTransaction;

import java.util.List;

public class TransactionResponse {
    @JsonProperty("status")
    private String status;

    @JsonProperty("message")
    private String message;

    @JsonProperty("result")
    private List<WalletTransaction> result;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<WalletTransaction> getResult() {
        return result;
    }

    public void setResult(List<WalletTransaction> result) {
        this.result = result;
    }

    public TransactionResponse() {
    }

    public TransactionResponse(String status, String message, List<WalletTransaction> result) {
        this.status = status;
        this.message = message;
        this.result = result;
    }
}
