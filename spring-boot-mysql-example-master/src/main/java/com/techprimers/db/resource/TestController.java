package com.techprimers.db.resource;

import com.techprimers.db.model.User;
import com.techprimers.db.model.Wallet;
import com.techprimers.db.model.WalletTransaction;
import com.techprimers.db.model.response.TransactionResponse;
import com.techprimers.db.repository.UserRepository;
import com.techprimers.db.repository.WalletRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@RestController
@RequestMapping(value = "/rest/users")
public class TestController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    WalletRepository walletRepository;

    private static final String API_KEY = "QXMRKCRCEUPIDQ9UZ9TK4T9RWJ3DSP56RD";
    private static final String GET_TRANSACTIONS_STATUS_OK = "1";

    @GetMapping(value = "/start")
    public String getAll() {
        List<Integer> usersIds = userRepository.getUsersIdList();
        usersIds.forEach(this::getAndParseUserById);
        return "ok";
    }

    private void getAndParseUserById(Integer id) {
        User user = userRepository.findOne(id);
        List<Wallet> userWallets = user.getWallets();
        userWallets.forEach(this::tryWalletTransaction);
    }

    private void tryWalletTransaction(Wallet wallet) {
        String url = "http://api.etherscan.io/api?module=account&action=txlist&address="
                + wallet.getAddress() + "&startblock=0&endblock=99999999&sort=asc&apikey=" + API_KEY;
        RestTemplate restTemplate = new RestTemplate();
        TransactionResponse result = restTemplate.getForObject(url, TransactionResponse.class);
        if (result.getStatus().equals(GET_TRANSACTIONS_STATUS_OK) && result.getResult().size() > 0) {
            trySaveTransaction(wallet, result.getResult());
        }
    }

    private void trySaveTransaction(Wallet wallet, List<WalletTransaction> responseTransactionList) {
        List<WalletTransaction> walletTransactions = wallet.getTransactions();
        if (!walletTransactions.isEmpty()) {
            WalletTransaction newestWalletTransaction = walletTransactions.get(walletTransactions.size() - 1);
            WalletTransaction newestResponseTransaction = responseTransactionList.get(0);
            if (!newestWalletTransaction.getTimeStamp().equals(newestResponseTransaction.getTimeStamp())) {
                for (WalletTransaction responseTransaction : responseTransactionList) {
                    if (responseTransaction.getTimeStamp().equals(newestWalletTransaction.getTimeStamp())) {

                        wallet.getTransactions().add(responseTransaction);

                    } else {
                        break;
                    }
                }

            }
        }else {
            wallet.getTransactions().addAll(responseTransactionList);
            walletRepository.save(wallet);
        }
    }
}
