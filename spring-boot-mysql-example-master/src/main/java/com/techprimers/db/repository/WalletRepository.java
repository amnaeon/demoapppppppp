package com.techprimers.db.repository;

import com.techprimers.db.model.Wallet;
import org.springframework.data.repository.CrudRepository;

public interface WalletRepository extends CrudRepository<Wallet,Integer>{
}
