package com.techprimers.db.repository;
import com.techprimers.db.model.User;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface UserRepository extends CrudRepository<User,Integer> {
    @Query("select user.id from User as user")
    List<Integer> getUsersIdList();
}
